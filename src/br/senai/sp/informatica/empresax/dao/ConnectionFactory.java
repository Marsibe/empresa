package br.senai.sp.informatica.empresax.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	public Connection getConnection() {
		try {
			// registra o driver jdbc
			Class.forName("com.mysql.jdbc.Driver");
			// retorna um objeto java.sql.connection
			// abre uma conex�o com o banco de dados
			return DriverManager.getConnection("jdbc:mysql://172.16.7.23/empresat", "tarde", "tarde");
		}catch(SQLException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
