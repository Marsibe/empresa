package br.senai.sp.informatica.empresax.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.empresax.model.Funcionario;

public class FuncionarioDao {
	// Atributo
		private Connection connection;

		// Construtor
		public FuncionarioDao() {
			// Estabelece uma conex�o com o banco de dados
			connection = new ConnectionFactory().getConnection();
		}

		// Salva
		public void salva(Funcionario funcionario) {
			String sql = null;
			
			if (funcionario.getId() != null) {
				sql = "UPDATE funcionario SET nome = ?, email = ?, cpf = ?, "
						+ "senha = ? WHERE id = ?";
			} else {
				// Cria um comando sql
				sql = "INSERT INTO funcionario " + "(nome, email, cpf, senha) " 
				+ "VALUES (?, ?, ?, ?)";
			}

			try {
				// Cria um preparedstatement
				PreparedStatement stmt = connection.prepareStatement(sql);

				// Cria os par�metros do PreparedStatement
				stmt.setString(1, funcionario.getNome());
				stmt.setString(2, funcionario.getEmail());
				stmt.setString(3, funcionario.getCpf());
				stmt.setString(4, funcionario.getSenha());
				if (funcionario.getId() != null) {
					stmt.setLong(5, funcionario.getId());
				}

				// executa o insert
				stmt.execute();

				// libera o recurso statement
				stmt.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}finally {
				try {
				 connection.close();
				}catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}

		}

		// getlista
		public List<Funcionario> getLista() {
			try {
				// Cria um ArrayList
				List<Funcionario> funcionarios = new ArrayList<>();

				// Cria um PreparedStatement
				PreparedStatement stmt = connection.prepareStatement("SELECT * FROM funcionario");

				// executa o statement e guarda o resultado em um resultset
				ResultSet rs = stmt.executeQuery();

				// enquanto houver dados no resultset
				while (rs.next()) {
					// cria um contato com os dados do resultset
					Funcionario funcionario = new Funcionario();
					funcionario.setId(rs.getLong("id"));
					funcionario.setNome(rs.getString("nome"));
					funcionario.setEmail(rs.getString("email"));
					funcionario.setCpf(rs.getString("cpf"));
					funcionario.setSenha(rs.getString("senha"));
					funcionarios.add(funcionario);
				}

				rs.close();

				stmt.close();

				return funcionarios;
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}finally {
				try {
					connection.close();
				}catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		public void excluir(Funcionario funcionario) {
			try {
				PreparedStatement stmt = connection.prepareStatement("DELETE FROM funcionario WHERE id = ?");
				stmt.setLong(1, funcionario.getId());
				stmt.execute();
				stmt.close();
			}catch (SQLException e) {
				throw new RuntimeException(e);
			}finally {
				try {
					connection.close();
				}catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		}
}
