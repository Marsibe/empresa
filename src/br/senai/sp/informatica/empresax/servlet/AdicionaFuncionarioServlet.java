package br.senai.sp.informatica.empresax.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresax.dao.FuncionarioDao;
import br.senai.sp.informatica.empresax.model.Funcionario;

@SuppressWarnings("serial")
@WebServlet("/adicionaFuncionario")
public class AdicionaFuncionarioServlet extends HttpServlet {
	
		// Sobrescreve o m�todo service
		@Override
		protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
			
			// Obt�m um objeto do tipo printwriter do response
			// �til para escrever mensagens no navegador do usu�rio
			PrintWriter out = res.getWriter();
			
			// Pega os parametros do formul�rio (request)
			String nome = req.getParameter("nome");
			String email = req.getParameter("email");
			String cpf = req.getParameter("cpf");
			String senha = req.getParameter("senha");
			
			Funcionario funcionario = new Funcionario();
			funcionario.setNome(nome);
			funcionario.setEmail(email);
			funcionario.setCpf(cpf);
			funcionario.setSenha(senha);
			
			// obt�m uma instancia de ContatoDao
			// e abre uma conex�o com o banco de dados
			FuncionarioDao dao = new FuncionarioDao();
			
			// salva o contato no banco de dados
			dao.salva(funcionario);
			
			// feedback para o usu�rio
			out.println("<html>");
			out.println("<body>");
			out.println("Funcionario " + funcionario.getNome() + " salvo com sucesso !");
			out.println("</body>");
			out.println("</html>");
		}
}
