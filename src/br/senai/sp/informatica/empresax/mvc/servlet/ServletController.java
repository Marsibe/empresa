package br.senai.sp.informatica.empresax.mvc.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresax.mvc.logica.Logica;

@SuppressWarnings("serial")
@WebServlet("/mvc")
public class ServletController extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// pega o parametro logica da requisi��o
		String parametro = req.getParameter("logica");
		
		// monta o nome da classe de logica que deve
		// ser executada
		String className = "br.senai.sp.informatica.empresax.mvc.logica." + parametro;
		
		try {
			// carrega a classe em memoria
			@SuppressWarnings("rawtypes")
			Class classe = Class.forName(className);
			// cria uma instancia da logica
			Logica logica = (Logica) classe.newInstance();
			// cria a p�gina para redirecionar
			String pagina = logica.executa(req, res);
			// encaminhar o usuario
			req.getRequestDispatcher(pagina).forward(req, res);
		}catch (Exception e) {
			throw new ServletException("A l�gica de negocios causou uma exce��o", e);
		}
	}
}
