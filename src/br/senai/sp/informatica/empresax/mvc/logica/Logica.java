package br.senai.sp.informatica.empresax.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Logica {
	// m�todo que executa a logica
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception;
}
