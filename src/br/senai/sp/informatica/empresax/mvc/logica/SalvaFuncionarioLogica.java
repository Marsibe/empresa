package br.senai.sp.informatica.empresax.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresax.dao.FuncionarioDao;
import br.senai.sp.informatica.empresax.model.Funcionario;

public class SalvaFuncionarioLogica implements Logica{

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String cpf = req.getParameter("cpf");
		String senha = req.getParameter("senha");
		
		// cria uma instancia de contato com 
		// os dados do formulario
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setCpf(cpf);
		funcionario.setSenha(senha);
		
		FuncionarioDao dao = new FuncionarioDao();
		
		// chama o metodo para salvar o contato
		dao.salva(funcionario);
		
		// feedback para o usuario
		return "/WEB-INF/jsp/funcionario-adicionado.jsp";
	}
	
}
