package br.senai.sp.informatica.empresax.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresax.dao.FuncionarioDao;
import br.senai.sp.informatica.empresax.model.Funcionario;

public class RemoveFuncionarioLogica implements Logica{

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		long id = Long.parseLong(req.getParameter("id"));
		
		Funcionario funcionario = new Funcionario();
		funcionario.setId(id);
		
		FuncionarioDao dao = new FuncionarioDao();
		dao.excluir(funcionario);
		
		System.out.println("Excluindo o funcionario...");
		
		// apos excluir redireciona para listar
		return "mvc?logica=ListaFuncionariosLogica";
	}
	
}
