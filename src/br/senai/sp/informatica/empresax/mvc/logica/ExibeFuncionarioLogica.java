package br.senai.sp.informatica.empresax.mvc.logica;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresax.dao.FuncionarioDao;
import br.senai.sp.informatica.empresax.model.Funcionario;

public class ExibeFuncionarioLogica implements Logica{

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		Funcionario funcionario = null;
		
		long id = Long.parseLong(req.getParameter("id"));
		
		List<Funcionario> funcionarios = new FuncionarioDao().getLista();
		
		for(Funcionario f : funcionarios) {
			if(f.getId() == id) {
				funcionario = f;
				break;
			}
		}
		
		req.setAttribute("id", funcionario.getId());
		req.setAttribute("nome", funcionario.getNome());
		req.setAttribute("email", funcionario.getEmail());
		req.setAttribute("cpf", funcionario.getCpf());
		req.setAttribute("senha", funcionario.getSenha());
		
		return "/WEB-INF/jsp/altera-funcionario.jsp";
	}
	
}
