<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="senai"%>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
	<meta charset="UTF-8">
	<title>Empresax - Cadastro de Funcionario</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	</head>
	<body>
		<c:import url="cabecalho.jsp" />
		<div class="container">
		<form action="mvc?logica=EditaFuncionarioLogica&id=${requestScope.id }" method="post">
			Nome:
			<br/>
			<input type="text" name="nome" value="${requestScope.nome }" />
			<br/><br/>
			E-mail:
			<br/>
			<input type="text" name="email" value="${requestScope.email }" />
			<br/><br/>
			CPF:
			<br/>
			<input type="text" name="cpf" value="${requestScope.cpf }" />
			<br/><br/>
			Senha:
			<br/>
			<input type="password" name="senha" value="${requestScope.senha }" />
			<br/><br/>	
			<input type="submit" name="Salvar" />
		</form>
		</div>
		<c:import url="rodape.jsp" />
	</body>
</html>