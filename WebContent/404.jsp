<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="senai"%>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
	<meta charset="UTF-8">
	<title>Empresax - P�gina n�o encontrada</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	</head>
	<body>
		<c:import url="cabecalho.jsp" />
		<div class="lado">
		<h1>A p�gina n�o existe !</h1>
		</div>
		<c:import url="rodape.jsp" />
	</body>
</html>