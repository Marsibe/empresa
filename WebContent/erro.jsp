<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="senai"%>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
	<meta charset="UTF-8">
	<title>Empresax - Erro</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	</head>
	<body>
		<c:import url="cabecalho.jsp" />
		<div class="container">
		<h1>Bad Bad Server !</h1>
		<h3>Algo deu errado...</h3>
		<img alt="homer" src="imagens/homer.png" width="150">
		</div>
		<c:import url="rodape.jsp" />
	</body>
</html>